<#include "../parts/main-template.ftl">
<#import "/spring.ftl" as spring />
<@main "Add client"/>
<#macro content>

    <@spring.bind "clientInfo"/>
    <form class="ui form client-form" action="../clients/save" method="post">
        <div class="ui internally celled grid">
            <div class="row">
                <div class="eight wide column">

                    <h4 class="ui dividing header">Main Information</h4>
                    <#assign isNotEmpty = clientInfo.series??>
                    <@spring.formInput "clientInfo.clientId" "" "hidden"/>
                    <@spring.formInput "clientInfo.passportDataId" "" "hidden"/>
                    <div class="field">
                        <label>Last Name</label>
                        <@spring.formInput "clientInfo.lastName" "placeholder='Last Name' autocomplete='off'" "text"/>
                    </div>
                    <div class="field">
                        <label>First Name</label>
                        <@spring.formInput "clientInfo.firstName" "placeholder='First Name' autocomplete='off'" "text"/>
                    </div>
                    <div class="field">
                        <label>Patronymic</label>
                        <@spring.formInput "clientInfo.patronymic" "placeholder='Patronymic' autocomplete='off'" "text"/>
                    </div>
                    <div class="field">
                        <label>Sex</label>
                        <div class="ui selection dropdown">
                            <@spring.bind "clientInfo.sex"/>
                            <input type="hidden" id="${spring.status.expression?replace('[','')?replace(']','')}"
                                   name="${spring.status.expression}"
                                   <#if isNotEmpty>value="${clientInfo.sex}"</#if>>
                            <i class="dropdown icon"></i>
                            <div class="default text">Sex</div>
                            <div class="menu">
                                <div class="item" data-value="male" data-text="Male">
                                    Male
                                </div>
                                <div class="item" data-value="female" data-text="Female">
                                    Female
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="field phones-list">
                        <label>Phones</label>
                        <#if isNotEmpty>
                            <#list clientInfo.phones as phone>
                                <div class="field">
                                    <input type="hidden" name="phoneId" value="${phone.id}">
                                    <input type="tel" name="phone" placeholder="Phone number" class="phone"
                                           autocomplete="off" value="${phone.phone}">
                                </div>
                            </#list>
                        <#else>
                            <div class="field">
                                <input type="tel" name="phone" placeholder="Phone number" class="phone"
                                       autocomplete="off">
                            </div>
                        </#if>
                    </div>
                    <a class="add-button">+ Add</a>
                </div>

                <div class="eight wide column">
                    <h4 class="ui dividing header">Passport Data</h4>
                    <div class="field">
                        <label>Passport Series</label>
                        <@spring.formInput
                        "clientInfo.series" "placeholder='Passport Series' autocomplete='off' maxlength='2'" "text"/>
                    </div>
                    <div class="field">
                        <label>Passport ID</label>
                        <@spring.formInput
                        "clientInfo.number" "placeholder='Passport ID' autocomplete='off' maxlength='6'" "text"/>
                    </div>
                    <div class="field">
                        <label>Issue Place</label>
                        <@spring.formInput
                        "clientInfo.issuePlace" "placeholder='Issue Place' autocomplete='off'" "text"/>
                    </div>
                    <div class="field">
                        <label>Issue Date</label>
                        <div class="ui calendar">
                            <@spring.formInput
                            "clientInfo.issueDate" "placeholder='Issue Date' autocomplete='off'" "text"/>
                        </div>
                    </div>
                    <div class="field">
                        <label>Birth Date</label>
                        <div class="ui calendar">
                            <@spring.formInput
                            "clientInfo.birthdate" "placeholder='Birth Date' autocomplete='off'" "text"/>
                        </div>
                    </div>
                    <div class="field">
                        <label>Birth Place</label>
                        <@spring.formInput
                        "clientInfo.birthPlace" "placeholder='Birth Place' autocomplete='off'" "text"/>
                    </div>
                    <div class="field">
                        <label>Registration</label>
                        <@spring.formTextarea
                        "clientInfo.registration" "rows='2'"/>
                    </div>

                </div>
            </div>
        </div>
        <input type="hidden" name="_csrf" value="${_csrf.token}"/>
        <button type="submit" class="ui primary button">${action}</button>
    </form>
</#macro>