<#include "../parts/main-template.ftl">
<@main "Client's info"/>
<#macro content>

    <table class="ui very basic padded table">
        <tr>
            <td><strong>Full Name:</strong></td>
            <td>${clientInfo.lastName} ${clientInfo.firstName} ${clientInfo.patronymic}</td>
        </tr>
        <tr>
            <td><strong>Birth Date:</strong></td>
            <td>${clientInfo.birthdate}</td>
        </tr>
        <tr>
            <td><strong>Birth Place:</strong></td>
            <td>${clientInfo.birthPlace}</td>
        </tr>
        <tr>
            <td><strong>Sex:</strong></td>
            <td>${clientInfo.sex}</td>
        </tr>
        <tr>
            <td><strong>Registration:</strong></td>
            <td>${clientInfo.registration}</td>
        </tr>
        <tr>
            <td><strong>Passport:</strong></td>
            <td>${clientInfo.series} ${clientInfo.number}</td>
        </tr>
        <tr>
            <td><strong>Issue Date:</strong></td>
            <td>${clientInfo.issueDate}</td>
        </tr>
        <tr>
            <td><strong>Issue Place:</strong></td>
            <td>${clientInfo.issuePlace}</td>
        </tr>
        <tr>
            <td><strong>Phones:</strong></td>
            <td>
                <#list clientInfo.phones as phone>
                    <p>${phone.phone}</p>
                </#list>
            </td>
        </tr>
        <tr>
            <td><strong>Tickets:</strong></td>
            <td><a href="../../tickets/clients-tickets?id=${clientInfo.clientId}">View</a></td>
        </tr>
    </table>

    <div>
        <a href="../../clients/edit?id=${clientInfo.clientId}"
           class="ui floated small labeled icon button">
            <i class="edit icon"></i>
            Edit
        </a>
    </div>

</#macro>