package ua.nmu.airportticketoffice.service;

import ua.nmu.airportticketoffice.entity.Client;
import ua.nmu.airportticketoffice.service.dto.ClientDTO;

import java.util.List;

public interface ClientService {
    Client save(Client client);

    Client save(ClientDTO client, Integer[] phonesIds, String[] phones);

    List<Client> saveAll(List<Client> clients);

    List<Client> findAll();

    List<Client> findAllOrderByLastNameAsc();

    ClientDTO findById(int id);

    void delete(Client client);

    void deleteById(int id);
}
