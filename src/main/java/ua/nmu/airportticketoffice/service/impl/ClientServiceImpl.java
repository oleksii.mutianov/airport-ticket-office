package ua.nmu.airportticketoffice.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ua.nmu.airportticketoffice.entity.Client;
import ua.nmu.airportticketoffice.entity.PassportData;
import ua.nmu.airportticketoffice.entity.Phone;
import ua.nmu.airportticketoffice.repository.ClientRepository;
import ua.nmu.airportticketoffice.service.ClientService;
import ua.nmu.airportticketoffice.service.PassportDataService;
import ua.nmu.airportticketoffice.service.dto.ClientDTO;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class ClientServiceImpl implements ClientService {

    private ClientRepository clientRepository;
    private PassportDataService passportDataService;

    @Override
    public List<Client> findAllOrderByLastNameAsc() {
        return clientRepository.findAllByOrderByLastNameAsc();
    }

    @Override
    public ClientDTO findById(int id) {
        Client client = clientRepository.findById(id).get();

        return convertToClientDTO(client);
    }

    private ClientDTO convertToClientDTO(Client client) {
        ClientDTO clientDTO = new ClientDTO();

        clientDTO.setClientId(client.getId());
        clientDTO.setFirstName(client.getFirstName());
        clientDTO.setLastName(client.getLastName());
        clientDTO.setPatronymic(client.getPatronymic());
        clientDTO.setPhones(client.getPhones());

        clientDTO.setPassportDataId(client.getPassportData().getId());
        clientDTO.setSeries(client.getPassportData().getSeries());
        clientDTO.setNumber(client.getPassportData().getNumber());
        clientDTO.setBirthdate(client.getPassportData().getBirthdate());
        clientDTO.setBirthPlace(client.getPassportData().getBirthPlace());
        clientDTO.setSex(client.getPassportData().getSex());
        clientDTO.setIssuePlace(client.getPassportData().getIssuePlace());
        clientDTO.setIssueDate(client.getPassportData().getIssueDate());
        clientDTO.setRegistration(client.getPassportData().getRegistration());

        return clientDTO;
    }

    @Override
    public void delete(Client client) {
        clientRepository.delete(client);
    }

    @Override
    public Client save(Client client) {
        return clientRepository.save(client);
    }

    @Override
    public Client save(ClientDTO clientDTO, Integer[] phonesIds, String[] phones) {
        Client client = convertFromClientDTO(clientDTO);
        setPhones(phonesIds, phones, client);

        return clientRepository.save(client);
    }

    private Client convertFromClientDTO(ClientDTO clientDTO) {

        Client client = new Client();
        PassportData passportData = new PassportData();

        if (clientDTO.getClientId() != 0) {
            client = clientRepository.findById(clientDTO.getClientId()).get();
            passportData = client.getPassportData();
        }

        client.setFirstName(clientDTO.getFirstName());
        client.setLastName(clientDTO.getLastName());
        client.setPatronymic(clientDTO.getPatronymic());

        passportData.setSeries(clientDTO.getSeries());
        passportData.setNumber(clientDTO.getNumber());
        passportData.setBirthdate(clientDTO.getBirthdate());
        passportData.setBirthPlace(clientDTO.getBirthPlace());
        passportData.setSex(clientDTO.getSex());
        passportData.setIssuePlace(clientDTO.getIssuePlace());
        passportData.setIssueDate(clientDTO.getIssueDate());
        passportData.setRegistration(clientDTO.getRegistration());

        passportData = passportDataService.save(passportData);

        client.setPassportData(passportData);

        return client;
    }

    private void setPhones(Integer[] ids, String[] phones, Client client) {
        List<Phone> clientPhones = new ArrayList<>(phones.length);

        for (int i = 0; i < phones.length; i++) {
            Phone phone = new Phone();
            if (ids != null && i < ids.length) {
                phone.setId(ids[i]);
            } else {
                phone.setId(0);
            }
            phone.setPhone(phones[i]);
            phone.setClient(client);
            clientPhones.add(phone);
        }
        client.setPhones(clientPhones);
    }

    @Override
    public List<Client> saveAll(List<Client> clients) {
        return clientRepository.saveAll(clients);
    }

    @Override
    public List<Client> findAll() {
        return clientRepository.findAll();
    }

    @Override
    public void deleteById(int id) {
        clientRepository.deleteById(id);
    }
}
