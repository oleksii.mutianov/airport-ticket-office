package ua.nmu.airportticketoffice.service.dto;

import lombok.Getter;
import lombok.Setter;
import ua.nmu.airportticketoffice.entity.Phone;

import java.sql.Date;
import java.util.List;

@Getter
@Setter
public class ClientDTO {
    private int clientId;

    private String firstName;

    private String lastName;

    private String patronymic;

    private int passportDataId;

    private String series;

    private String number;

    private Date birthdate;

    private String birthPlace;

    private String sex;

    private String issuePlace;

    private Date issueDate;

    private String registration;

    private List<Phone> phones;
}
