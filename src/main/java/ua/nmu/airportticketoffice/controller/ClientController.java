package ua.nmu.airportticketoffice.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ua.nmu.airportticketoffice.entity.Client;
import ua.nmu.airportticketoffice.service.ClientService;
import ua.nmu.airportticketoffice.service.dto.ClientDTO;

import java.util.List;

@Controller
@RequestMapping({"/clients", "/"})
@AllArgsConstructor
public class ClientController {

    private ClientService clientService;

    @RequestMapping("/")
    public String start() {
        return "redirect:/clients/list";
    }

    @GetMapping("/list")
    public String listClients(Model model) {

        List<Client> clients = clientService.findAllOrderByLastNameAsc();

        model.addAttribute("clients", clients);

        return "/clients/list-clients";
    }

    @GetMapping("/client-info")
    public String clientInfo(@RequestParam int id, Model model) {

        ClientDTO client = clientService.findById(id);

        model.addAttribute("clientInfo", client);

        return "/clients/client-info";
    }

    @GetMapping("/add")
    public String showFormForAdd(Model model) {

        ClientDTO clientDTO = new ClientDTO();

        model.addAttribute("clientInfo", clientDTO);
        model.addAttribute("action", "Add");

        return "/clients/client-form";
    }

    @GetMapping("/edit")
    public String showFormForEdit(@RequestParam int id, Model model) {

        ClientDTO clientDTO = clientService.findById(id);
        model.addAttribute("clientInfo", clientDTO);
        model.addAttribute("action", "Update");

        return "/clients/client-form";
    }

    @PostMapping("/save")
    public String saveClient(
            @ModelAttribute("clientInfo") ClientDTO clientDTO,
            @RequestParam(name = "phone") String[] phones,
            @RequestParam(name = "phoneId", required = false) Integer[] phoneIds
    ) {

        clientService.save(clientDTO, phoneIds, phones);

        return "redirect:/clients/list";
    }

    @GetMapping("/delete")
    public String deleteEmployee(@RequestParam int id) {

        clientService.deleteById(id);

        return "redirect:/clients/list";
    }

}
