package ua.nmu.airportticketoffice.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "clients")
@NoArgsConstructor
@ToString(of = {"id", "lastName", "firstName", "patronymic"})
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "patronymic")
    private String patronymic;

    @OneToOne(fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "passport_data_id")
    private PassportData passportData;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "client",
            cascade  = CascadeType.ALL
    )
    private List<Phone> phones;

    @OneToMany(fetch = FetchType.LAZY,
            mappedBy = "client",
            cascade  = CascadeType.ALL
    )
    private List<Ticket> tickets;
}
