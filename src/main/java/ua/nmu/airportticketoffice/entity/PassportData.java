package ua.nmu.airportticketoffice.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "passport_data")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class PassportData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "series")
    private String series;

    @Column(name = "number")
    private String number;

    @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "birth_place")
    private String birthPlace;

    @Column(name = "sex")
    private String sex;

    @Column(name = "issue_place")
    private String issuePlace;

    @Column(name = "issue_date")
    private Date issueDate;

    @Column(name = "registration")
    private String registration;

    @OneToOne(mappedBy = "passportData")
    private Client client;

    @Override
    public String toString() {
        return "PassportData{" +
                "id=" + id +
                ", series='" + series + '\'' +
                ", number=" + number +
                ", birthdate=" + birthdate +
                ", birthPlace='" + birthPlace + '\'' +
                ", sex=" + sex +
                ", issuePlace='" + issuePlace + '\'' +
                ", issueDate=" + issueDate +
                ", note='" + registration + '\'' +
                ", client id=" + client.getId() +
                '}';
    }
}
