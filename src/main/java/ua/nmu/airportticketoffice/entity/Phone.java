package ua.nmu.airportticketoffice.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "phones")
@NoArgsConstructor
@ToString(of = {"id", "phone"})
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "phone")
    private String phone;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.DETACH, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "client_id")
    private Client client;

}
