package ua.nmu.airportticketoffice.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "cities")
@NoArgsConstructor
@ToString(of = {"id", "name"})
@Getter
@Setter
public class City {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @OneToMany(mappedBy = "departureCity", cascade = CascadeType.ALL)
    private List<Flight> flight1;

    @OneToMany(mappedBy = "arrivalCity", cascade = CascadeType.ALL)
    private List<Flight> flight2;
}
